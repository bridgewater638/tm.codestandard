# tm.codestandard

##Naming convention
###1. Project: use underscore 
`my_project_name`

###2. Folder: use lowercase 
`js, css`

###3. JS:
`sm.js or special_market.js`

###4. CSS:  
`sm.scss or special_market.scss`

###5. HTML:
`special_market.html`


##HTML
###1. Syntax:
####*indentation
####*use double quote for attribute
####*check closing tag
```
<!DOCTYPE html>
<html>
    <head>
        <title>Page title</title>
    </head>
    <body>
        <img src="images/company_logo.png" alt="Company"/>
        <h1 class="hello-world">Hello, world!</h1>
    </body>
</html>
```

###2. CSS/JS Import:
```
<!-- External CSS for special market-->
<link rel="stylesheet" href="special_market.css">

<!-- In-document CSS -->
<style>
    ...
</style>

<!-- External JS -->
<script src="special_market.js"></script>

<!-- In-document JS -->
<script>
    ...
</script>

```

###3. Attribute sequence order:

####id/class/name/data-*/src-href-value-max-length/placeholder/aria-*/required

```
<a class="..." id="..." data-modal="toggle" href="#">Example link</a>

<input class="form-control" type="text">

<img src="..." alt="...">

```

###5. Minimize number of tags
```
<!-- Not well -->
<span class="avatar">
    <img src="...">
</span>

<!-- Better -->
<img class="avatar" src="...">

```


##CSS
###1. tag/;
```
.element {
    position: absolute;
    top: 10px;
    left: 10px;

    border-radius: 10px;
    width: 50px;
    height: 50px;
}

```

###2. space
```
/* not good */
.element ,
.dialog{
    ...
}

/* good */
.element,
.dialog {

}

/* not good */
.element>.dialog{
    ...
}

/* good */
.element > .dialog{
    ...
}

```

###3. !important
#### * You can not use !important


###4. Comment
```
/* Modal header */
.modal-header {
    ...
}

/*
 * Modal header
 */
.modal-header {
    ...
}

.modal-header {
    /* 50px */
    width: 50px;

    color: red; /* color red */
}


```

###5. Naming 
#### variable/function/placeholder use camelcase
```
/* class */
.element-content {
    ...
}

/* id */
#myDialog {
    ...
}

/* variable */
$colorBlack: #000;

/* function */
@function pxToRem($px) {
    ...
}

```

###6. Attribute declare order
```
.declaration-order {
    display: block;
    float: right;

    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 100;

    border: 1px solid #e5e5e5;
    border-radius: 3px;
    width: 100px;
    height: 100px;

    font: normal 13px "Helvetica Neue", sans-serif;
    line-height: 1.5;
    text-align: center;

    color: #333;
    background-color: #f5f5f5;

    opacity: 1;
}

```


##Javascript
###1. Colon
```
/* var declaration */
var x = 1;

/* expression statement */
x++;

/* do-while */
do {
    x++;
} while (x < 10);

```

###2. Qutoe
```
// not good
var x = "test";

// good
var y = 'foo',
    z = '<div id="test"></div>';

```

###3. Naming
```
var thisIsMyName;

var goodID;

var reportURL;

var AndroidVersion;

var iOSVersion;

var MAX_COUNT = 10;

function Person(name) {
    this.name = name;
}

// not good
var body = $('body');

// good
var $body = $('body');

```

###4. Undefined
```
// not good
if (person === undefined) {
    ...
}

// good
if (typeof person === 'undefined') {
    ...
}

```

